import{HttpClient, json} from 'aurelia-fetch-client';

export abstract class DataService{
    
    
    protected constructor(private httpClient: HttpClient) {
        this.configureHttpClient();
        
    }

    protected get<TResault>(endpoint: string) : Promise<TResault>{
        return this.createRequest('GET', endpoint, null);
    }

    protected post<TResault>(endpoint : string, data : any) : Promise<TResault>{
        return this.createRequest('POST', endpoint, data);
    }

    protected put<TResault>(endpoint : string, data : any) : Promise<TResault>{
        return this.createRequest('PUT', endpoint, data);
    }

    protected delete<TResault>(endpoint : string) : Promise<TResault>{
        return this.createRequest('DELETE', endpoint, null);
    }

    private createRequest<TResault>(method: string, endpoint: string, data: any) : Promise<TResault> {
        let requestInit : RequestInit = {
            method: method
        };

        if(data !== null)
            requestInit.body = json(data);

        return this.httpClient.fetch(endpoint, requestInit).then(response => response.json());

    }

    private configureHttpClient(){
        this.httpClient.configure(config => {
            config.withBaseUrl('http://localhost:5000')
        });
    }
}